# Bitbucket Pipelines Pipe: Jira Create Issue

With this pipe you can create a [Jira][Jira] issue from a Bitbucket Pipeline

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.8.1
    variables:
      JIRA_BASE_URL: "<string>"
      JIRA_USER_EMAIL: "<string>"
      JIRA_API_TOKEN: "<string>"
      JIRA_PROJECT: "<string>"
      JIRA_ISSUE_SUMMARY: "<string>"
      # JIRA_ASSIGNEE: "<string>" # Optional
      # JIRA_ISSUE_TYPE: "<string>" # Optional
      # JIRA_ISSUE_DESCRIPTION: "<string>" # Optional
      # JIRA_ISSUE_FIRST_COMMENT: "<string>" # Optional
      # JIRA_CUSTOM_FIELDS: "<json>" # Optional.
      # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable                 | Usage                                                                                                                                                                               |
|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| JIRA_BASE_URL (**)       | URL of Jira instance. Example: https://<yourdomain>.atlassian.net.                                                                                                                  |
| JIRA_USER_EMAIL (**)     | Email of the user for which Access Token was created for. Example: human@example.com.                                                                                               |
| JIRA_API_TOKEN (**)      | [Access Token][api token] for Authorization.                                                                                                                                        |
| JIRA_PROJECT (*)         | Jira project key.                                                                                                                                                                   |
| JIRA_ISSUE_SUMMARY (*)   | Issue summary.                                                                                                                                                                      |
| JIRA_ASSIGNEE            | Issue assignee `accountId`. If a variable will not provided then the issue will be `Unassigned`. You could set this variable as `-1` to create the issue with `Automatic` assignee. |
| JIRA_ISSUE_TYPE          | Issue type. Default: `Task` (minimum issue type used in various Jira default projects (eg. core, software classic, software nextgen).                                               |
| JIRA_ISSUE_DESCRIPTION   | Issue details. Default: `Issue created from https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}`.         |
| JIRA_ISSUE_FIRST_COMMENT | Post an initial comment to the ticket with more information. Default: `"" (empty string)`.                                                                                          |
| JIRA_CUSTOM_FIELDS       | JSON document containing custom fields data.                                                                                                                                        |
| DEBUG                    | Turn on extra debug information. Default: `false`.                                                                                                                                  |

(*) = required variable. This variable needs to be specified always when using the pipe.
(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe.

## Prerequisites

To use this pipe you have to provide an API [Access Token][api token].

To find assignee `accountId` refer to these pages:

 - [Retrieve the Atlassian Account ID (AAID) in bitbucket.org][community blog assignee id],
 - [Find users via REST API][rest api assignee id].

To add custom fields to your jira issue type follow this link example in the UI:

- `<your_jira_url>/jira/software/projects/<your_project_id>/settings/issuetypes/<issue_type_id>`

To get custom fields keys for your jira issue type send request to the next REST API URL:

- `<your_jira_url>/rest/api/2/issue/createmeta/<your_project_id>/issuetypes/<issue_type_id>`


## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.8.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
```

Create an issue with assignee:

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.8.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
      JIRA_ASSIGNEE: "<accountId>"
```

Create an issue with automatic assignee:

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.8.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
      JIRA_ASSIGNEE: "-1"
```

Advanced example:

The `JIRA_ISSUE_DESCRIPTION` field allows the customization of the resulting Jira issue's description. Unless specified with `JIRA_ISSUE_DESCRIPTION_SUFFIX=False` there will always be a link back to the pipeline that created the ticket (eg. `"Issue created from https://bitbucket.org/{repo_owner}/{repo_slug}/addon/pipelines/home#!/results/{build_bumber}"`). In customizing this description there is support for string replacements to get similar functionality by including `{repo_owner}`, `{repo_slug}`, and/or `{build_number}` which will help to build links to the resulting pipeline or repository if desired.

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.8.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
      JIRA_ISSUE_TYPE: "Story"
      JIRA_ISSUE_DESCRIPTION: "This is a sample issue summary created from {repo_owner}/{repo_slug} and from the CI/CD build related here, https://bitbucket.org/{repo_owner}/{repo_slug}/addon/pipelines/home#!/results/{build_number}"
      JIRA_ISSUE_FIRST_COMMENT: "This is some content to put in the first comment."
      JIRA_ISSUE_DESCRIPTION_SUFFIX: False
```

Create an issue with custom fields (look at the `Prerequisites` to get info about how to add/retrieve custom fields):

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.8.1
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
      JIRA_CUSTOM_FIELDS: >
        {
            "customfield_10034": "Free text goes here.  Type away!",
            "customfield_10035": 42.07
        }
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## Contributions

Contributions to Jira Create Issue are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 


## License

Copyright (c) [2020] Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,jira
[Jira]: https://www.atlassian.com/software/jira
[api token]: https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/
[community blog assignee id]: https://community.atlassian.com/t5/Bitbucket-articles/Retrieve-the-Atlassian-Account-ID-AAID-in-bitbucket-org/ba-p/2471787
[rest api assignee id]: https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-user-search/#api-rest-api-2-user-search-get
