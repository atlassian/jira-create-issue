import os
import yaml

import jira

from bitbucket_pipes_toolkit import Pipe, get_current_pipeline_url


schema = {
    'JIRA_BASE_URL': {'type': 'string', 'required': True},
    'JIRA_USER_EMAIL': {'type': 'string', 'required': True},
    'JIRA_API_TOKEN': {'type': 'string', 'required': True},
    'JIRA_PROJECT': {'type': 'string', 'required': True},
    'JIRA_ISSUE_SUMMARY': {'type': 'string', 'required': True},
    'JIRA_ASSIGNEE': {'type': 'string', 'required': False},
    'JIRA_ISSUE_TYPE': {'type': 'string', 'required': False, 'default': 'Task'},
    'JIRA_ISSUE_DESCRIPTION': {'type': 'string', 'required': False, 'default': ""},
    'JIRA_ISSUE_DESCRIPTION_SUFFIX': {'type': 'boolean', 'required': False, 'default': True},
    'JIRA_ISSUE_FIRST_COMMENT': {'type': 'string', 'required': False, 'default': ""},
    'JIRA_CUSTOM_FIELDS': {'type': 'dict', 'required': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class JiraCreateIssuePipe(Pipe):
    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.jira_url = self.get_variable('JIRA_BASE_URL')
        self.email = self.get_variable('JIRA_USER_EMAIL')
        self.api_token = self.get_variable('JIRA_API_TOKEN')
        self.project = self.get_variable('JIRA_PROJECT')
        self.summary = self.get_variable('JIRA_ISSUE_SUMMARY')
        self.assignee = self.get_variable('JIRA_ASSIGNEE')
        self.issue_type = self.get_variable('JIRA_ISSUE_TYPE')
        self.first_comment = self.get_variable('JIRA_ISSUE_FIRST_COMMENT')
        self.custom_fields = self.get_variable('JIRA_CUSTOM_FIELDS')
        self.description_template = self.get_variable('JIRA_ISSUE_DESCRIPTION')
        self.debug = self.get_variable('DEBUG')

    def run(self):
        self.log_info('Executing the pipe...')

        description = self.format_description(self.description_template)

        self.log_info(f'formatted description {description}')

        # create an instance of the Jira
        jira_client_instance = self.get_jira_client_instance()

        self.log_info(f'Creating a new issue in the project: {self.project}')

        new_issue_fields = {
            "project": self.project,
            "summary": self.summary,
            "assignee": {"accountId": self.assignee},
            "issuetype": {'name': self.issue_type},
            "description": description
        }

        if self.custom_fields:
            new_issue_fields.update(self.custom_fields)

        try:
            new_issue = jira_client_instance.create_issue(**new_issue_fields)
        except jira.JIRAError as e:
            self.log_error(e.text)
            self.fail(message='Failed to create an issue')

        if self.first_comment:
            try:
                jira_client_instance.add_comment(new_issue.raw['id'], body=self.first_comment)
            except jira.JIRAError as e:
                self.log_error(e.text)
                self.fail(message='Failed to comment on issue')

        self.success(
            message=f'Successfully created a Jira issue. Follow this link to view the issue {new_issue.permalink()}')

    def get_jira_client_instance(self):
        self.log_info('Initializing a Jira client')

        try:
            jira_client_instance = jira.JIRA(
                self.jira_url,
                basic_auth=(self.email, self.api_token),
                max_retries=0,
                validate=True)
        except jira.JIRAError as e:
            self.log_error(e.text)
            self.fail(
                message=f'Failed to initialize a Jira client for {self.jira_url}')

        return jira_client_instance

    def format_description(self, template):
        description = template

        repo_owner = os.getenv('BITBUCKET_REPO_OWNER')
        repo_slug = os.getenv('BITBUCKET_REPO_SLUG')
        build_number = os.getenv('BITBUCKET_BUILD_NUMBER')

        if "{repo_owner}" in template:
            description = description.replace("{repo_owner}", repo_owner)
        if "{repo_slug}" in template:
            description = description.replace("{repo_slug}", repo_slug)
        if "{build_number}" in template:
            description = description.replace("{build_number}", build_number)

        suffix = self.get_variable('JIRA_ISSUE_DESCRIPTION_SUFFIX')
        if suffix:
            suffix_text = "Issue created from {}".format(get_current_pipeline_url())
            return '{}\n{}'.format(description, suffix_text)
        else:
            return description


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = JiraCreateIssuePipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
