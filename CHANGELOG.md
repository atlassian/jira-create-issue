# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.8.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.8.0

- minor: Internal maintenance: Bump version of jira package to 3.8.0

## 0.7.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.6.0

- minor: Add support for JIRA_ASSIGNEE variable. The feature to assign an issue to a user on create.
- minor: Add support for JIRA_CUSTOM_FIELDS variable. The feature to set custom fields to the issue on create.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 0.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.4.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 0.3.1

- patch: Internal maintenance: refactor tests to unit tests.

## 0.3.0

- minor: Add support to add first comment to jira issue.
- minor: Internal maintenance: Bump version of the base Docker image to python:3.10-slim.
- patch: Fix default value of JIRA_ISSUE_FIRST_COMMENT variable from None to empty string.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Bump version of the pipe's dependencies.
- patch: Internal maintenance: Bump versions of bitbucket-pipe-release and atlassian/default-image.
- patch: Internal maintenance: Fix tests.
- patch: Internal maintenance: Update community link.
- patch: Remove unnecessary parameter JIRA_FIRST_COMMENT from automation workflow.

## 0.2.0

- minor: Bump bitbucket-pipes-toolkit->2.2.0.

## 0.1.2

- patch: Internal maintenance: use absolute paths in docker image.

## 0.1.1

- patch: Internal maintenance: fix Dockerhub workspace.

## 0.1.0

- minor: Initial release of jira-create-issue pipe
