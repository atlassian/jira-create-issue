from http import HTTPStatus
import io
import os
import sys
from copy import copy
from contextlib import contextmanager
from jira import JIRAError
from unittest import TestCase

import pytest

from pipe.pipe import JiraCreateIssuePipe, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class CreateIssueCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker):
        self.caplog = caplog
        self.mocker = mocker

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_success(self):
        self.mocker.patch.dict(
            os.environ, {
                'JIRA_BASE_URL': 'https://test-url',
                'JIRA_USER_EMAIL': 'test-mail',
                'JIRA_API_TOKEN': 'test-token',
                'JIRA_PROJECT': 'test-project',
                'JIRA_ISSUE_SUMMARY': 'test-summary'
            }
        )

        jira_instance_mock = self.mocker.Mock()

        pipe = JiraCreateIssuePipe(
            schema=schema, check_for_newer_version=True)

        pipe.get_jira_client_instance = jira_instance_mock

        with capture_output() as out:
            pipe.run()

        self.assertIn('Successfully created a Jira issue', out.getvalue())

    def test_transition_failed(self):
        self.mocker.patch.dict(
            os.environ, {
                'JIRA_BASE_URL': 'https://test-url',
                'JIRA_USER_EMAIL': 'test-mail',
                'JIRA_API_TOKEN': 'test-token',
                'JIRA_PROJECT': 'test-project',
                'JIRA_ISSUE_SUMMARY': 'test-summary'
            }
        )

        jira_instance_mock = self.mocker.Mock()

        pipe = JiraCreateIssuePipe(
            schema=schema, check_for_newer_version=True)

        pipe.get_jira_client_instance = jira_instance_mock
        jira_instance_mock.return_value.create_issue.side_effect = JIRAError(
            text='Creation error', status_code=HTTPStatus.BAD_REQUEST)

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertIn('Failed to create an issue', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)

    def test_no_required_param_transition(self):
        self.mocker.patch.dict(
            os.environ, {
                'JIRA_BASE_URL': 'https://test-url',
                'JIRA_USER_EMAIL': 'test-mail',
                'JIRA_API_TOKEN': 'test-token',
                'JIRA_PROJECT': 'test-project'
            }
        )
        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                JiraCreateIssuePipe(
                    schema=schema, check_for_newer_version=True)

        self.assertIn('JIRA_ISSUE_SUMMARY:\n- required field', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)
